# Update exposure and contribution of humans to reservoir ----------------------
#' Update exposure and contribution of humans to reservoir
#'
#' \code{update_expo_contr_human} updates host exposure and contribution to
#' reservoir as a function of host age and potential uptake of interventions
#' that reduce exposure and/or contribution. Argument values can be passed down
#' via \code{...} in the \code{strongysim} function.
#'
#' @param humans \code{data.table} object containing the state of the human
#'   population.
#' @param time Time in years (refers by default to the time indicator \code{i}
#'   that is used within the \code{strongysim} function).
#' @param expo_age_x Age entries for age-dependent host exposure to reservoir.
#' @param expo_age_y Age-dependent host exposure to reservoir.
#' @param contr_age_x Age entries for age-dependent host contribution to
#'   reservoir.
#' @param contr_age_y Age-dependent host contribution to reservoir.
#' @param expo_contr_t Time points at which interventions change (years).
#' @param expo_contr_age Age group targeted by interventions.
#' @param expo_contr_uptake Proportion of people that take up interventions.
#' @param expo_eff Intervention effect on host exposure (1 = no effect, 0 = max
#'   effect).
#' @param contr_eff Intervention effect on host contribution (1 = no effect, 0 =
#'   max effect).
#'
#' @return This function does not return anything and relies on side-effects via
#'   the \code{data.table} structure.
#'
#' @export
update_expo_contr_human <- function(humans,
                                    time,
                                    expo_age_x,
                                    expo_age_y,
                                    contr_age_x,
                                    contr_age_y,
                                    expo_contr_t,
                                    expo_contr_age,
                                    expo_contr_uptake,
                                    expo_eff,
                                    contr_eff,
                                    ...) {

  # Update age-dependent exposure and contribution to reservoir
  humans[, rel_contr_age := approx(x = contr_age_x, y = contr_age_y,
                                   xout = age, rule = 2,
                                   method = "linear")[[2]]]
  humans[, rel_expo_age  := approx(x = expo_age_x, y = expo_age_y,
                                   xout = age, rule = 2,
                                   method = "linear")[[2]]]

  # Update impact of interventions on exposure and/or contribution
  humans[, expo_co := 1]
  humans[, contrib_co := 1]
  humans[findInterval(age, expo_contr_age) == 1L &
            ec_i <= expo_contr_uptake[findInterval(time, expo_contr_t)],
          c("expo_co", "contrib_co") :=
           list(rep(expo_eff[findInterval(time, expo_contr_t)], .N),
                rep(contr_eff[findInterval(time, expo_contr_t)], .N))]
  humans[, rel_contr := rel_contr_expo_i * rel_contr_age * contrib_co]
  humans[, rel_expo := rel_contr_expo_i * rel_expo_age * expo_co]

}


### END OF CODE ### ------------------------------------------------------------
