# Generate human agents --------------------------------------------------------
#' Generate human agents
#'
#' Generate human agents. Argument values can be passed down via \code{...} in
#' the \code{strongysim} function.
#'
#' @param n Number of new humans to be generated
#' @param age_table Ages to interpolate cumulative survival between (vector of
#'   equal lenght as surv_cum).
#' @param surv_cum Cumulative survival at ages in age_table (vector of equal
#'   lenght as age_table).
#' @param contrib_k Shape and rate parameter of gamma distribution (with mean
#'   1.0) for inter-individual variation in exposure and contribution to
#'   transmission.
#' @param rate_imm_comprom Probability to develop some sort of
#'   immune-compromised state at some point during their life, expressed in
#'   terms of rate per expected life-year. In the simulation, the actual
#'   probability is calculated as \code{1 - exp(-rate_imm_comprom *
#'   life_expectancy)}, where \code{life_expectancy} is the individual's life
#'   expectancy at birth. The actual timing of the development of an
#'   immune-compromised is determined by the argument \code{t_rel_imm_comprom}.
#' @param t_rel_imm_comprom Average time at which immuno-compromised state
#'   develops (relative to an individual's life expectancy).
#' @param dt Time step size in simulation, used to randomly schedule births
#'   across the period spanned by a single time step.
#' @param ... Arguments passed down via \code{...} in the \code{strongysim}
#'   function.
#'
#' @return Returns a \code{data.table} of human hosts.
#'
#' @export
gen_human <- function(n,
                      age_table,
                      surv_cum,
                      contrib_k,
                      rate_imm_comprom,
                      t_rel_imm_comprom,
                      dt,
                      ...) {

  age_death <- approx(x = surv_cum,
                      y = age_table,
                      xout = runif(n),
                      method = "linear")$y

  prop_imm_comprom <- 1 - exp(-rate_imm_comprom * age_death)

  data.table(sex = rbinom(n = n, size = 1, prob = 0.5),
             age = 0,
             age_death = age_death,
             age_imm_comprom = age_death *
               (1 - (runif(n) < prop_imm_comprom) * (1 - t_rel_imm_comprom)),
             rel_contr_expo_i = rgamma(n = n,
                                       shape = contrib_k,
                                       rate = contrib_k),
             rel_contr_age = 0,
             rel_expo_age = 0,
             rel_contr = 0,
             rel_expo = 0,
             expo_co = 1,       # effect of WASH / bednets if compliant
             contrib_co = 1,    # effect of WASH / bednets if compliant
             co_i = rnorm(n = n, mean = 0, sd = 1),  # MDA compliance factor
             ec_i = runif(n = n, min = 0, max = 1),  # WASH / bednet compliance factor
             foi = NA_real_,
             E = 0L,
             E_dur = NA_real_,
             I = 0L)
}


### END OF CODE ###

