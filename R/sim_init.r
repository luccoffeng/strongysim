# Initialise simulation --------------------------------------------------------
#' Initialise simulation
#'
#' \code{sim_init} initialises a list object containing the state of a host
#' population and an environmental reservoir of infection. Human demography will
#' be warmed up for a user-defined duration.
#'
#' @param n_human Number of humans.
#' @param human_warmup_duration Duration of warming up human demography (years).
#' @param human_warmup_steps Number of time steps per year for warming up human
#'   demography.
#' @param n_infection_init Number of initial infections that is seeded after
#'   warming up human demography.
#' @param ... Arguments passed down via \code{...} in the \code{strongysim}
#'   function.
#'
#' @export
sim_init <- function(n_human,
                     human_warmup_duration,
                     human_warmup_steps,
                     n_infection_init,
                     ...) {

  # Catch and adapt parameters for initialisation and warmup
  param <- list(...)

  # Warm up human demography
  human_warmup_duration <- human_warmup_duration * human_warmup_steps
  dt <- 1 / human_warmup_steps

  humans <- do.call(gen_human, c(n = n_human, dt = dt, param))

  humans[age < age_death,  # avoid crash
         age := runif(.N, min = age, max = age_death)]

  for(i in 1:human_warmup_duration) {
    humans[, age := age + dt]
    humans[age >= age_death, names(humans) :=
             do.call(gen_human, c(n = .N, dt = dt, param))]
  }
  do.call(update_expo_contr_human, c(humans = list(humans), time = 0, param))

  # Seed initial infections
  humans[sample.int(n = .N,
                    size = n_infection_init,
                    prob = rel_expo * age,  # weight also by age as infection is life-long
                    replace = FALSE),  # only assign unique hosts
         I := 1L]

  # Return result
  return(list(humans = humans, cloud_reservoir = 0))

}


### END OF CODE ### ------------------------------------------------------------
