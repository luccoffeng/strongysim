# strongysim

<!-- badges: start -->
<!-- badges: end -->

The goal of strongysim is to provide a user-friendly interface to perform stochastic simulations of transmission and control of Strongyloides stercoralis, a soil-transmitted helminth.

## Installation
You can install the latest released version of strongysim from GitLab:

``` r
remotes::install_gitlab("luccoffeng/strongysim")
```


## Example
``` r
rm(list = ls())

library(strongysim)
library(data.table)

?strongysim  # main page for package documentation with links to other help files

test <- strongysim(runtime = 10L,
                   steps = 360L,
                   survey_freq = 12L,
                   verbose = TRUE,
                   break_if_elim = TRUE,
                   zeta = 2,
                   n_human = 1e4)

```
